const template =
`<div class="row">
    <ul class="list-group list-group-flush">
      <li v-for="(message, index) in history" class="list-group-item message" v-if="!message.deleted">
        <div v-if="!inEditingMode(index)">
          <aside>
            <span class="author">{{ message.author }}</span>
            <span class="time">{{ moment(message.moment) }}</span>
            <span class="edited" v-if="message.edit">[edited]</span>
            <button class="btn btn-light" v-on:click="editMessage(index)" v-if="canEdit(message)">
              <i class="fas fa-edit"></i>
            </button>
            <button class="btn btn-light" v-on:click="deleteMessage(index)" v-if="canEdit(message)">
              <i class="fas fa-trash"></i>
            </button>
          </aside>
          <p>
            {{ content(message) }}
          </p>
        </div>
        <div v-if="inEditingMode(index)">
          <input-message
            v-bind:author="author"
            v-bind:edited-message="index"
            v-bind:default-value="content(message)" />
        </div>
      </li>
    </ul>
</div>`

Vue.component('messages', {
  props: ['author'],
  created() {
  },
  mounted() {
    this.poll = setInterval(() => {
      service.pull()
        .then(response => {
          this.history=response
        })
    } ,1000)

    this.$on('input-done', () => this.editedMessage = null)
  },
  methods: {
    canEdit(message) {
      return message.author == this.author
    },
    moment(momentString) {
      const date = new Date(momentString)
      return date.getHours() + ':' + date.getMinutes()
    },
    content(message) {
      return message.edit && message.edit.content || message.content;
    },
    inEditingMode(index) {
      return (this.editedMessage != null) && (this.editedMessage == index)
    },
    editMessage(key) {
      this.editedMessage = key
    },
    deleteMessage(key) {
      service.deleteMessage(key)
    }
  },
  data() {
    return {
      history: [],
      editedMessage: null
    }
  },
  template
})
