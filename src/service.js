const server = 'http://127.0.0.1:3000'

const deleteMessage = (key) =>
      fetch(`/message`, {
        method: 'delete',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          key
        })
      })

const pushAs = (author, message, key) =>
      fetch(`/message`, {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          key,
          author,
          content: message,
          moment: new Date()
        })
      })

const pull = () =>
      fetch(`/messages`)
      .then(response => response.text())
      .then(JSON.parse)

const pullParticipants = () =>
      fetch(`/participants`)
      .then(response => response.text())
      .then(JSON.parse)

const join = (author) =>
      fetch(`/participants/join`, {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          author
        })
      })

window.service = { pull, pushAs, deleteMessage, pullParticipants, join }
