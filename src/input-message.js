const inputMessageTemplate =
`<div class="card flex-row">
    <textarea class="form-control" placeholder="Message" v-model="message" rows="1" v-on:keyup.enter="sendMessage()" />
  </div>`

Vue.component('inputMessage', {
  props: ['author', 'editedMessage', 'defaultValue'],
  created() {
  },
  mounted() {
    this.message = this.defaultValue || ''
  },
  methods: {
    sendMessage() {
      service.pushAs(this.author, this.message, this.editedMessage)
        .then(response => {
          this.message = ''
          this.$parent.$emit('input-done', response)
        })
    }
  },
  data() {
    return {
      message: ''
    }
  },
  template: inputMessageTemplate
})
