var app = new Vue({
  el: '#app',
  created() {
  },
  mounted() {
    this.join = setInterval(() => {
      service.join(this.author || 'Anonymous')
    } ,1000)

    this.poll = setInterval(() => {
      service.pullParticipants()
        .then(response => {
          this.participants = response
          if (!this.author) {
            this.author = 'Anonymous ' + (Math.random() * 100).toFixed()
          }
        })
    } ,1000)
  },
  methods: {
  },
  computed: {
    participantCount() {
      return this.participants.length
    }
  },
  data: {
    // I change `isChatting` in the view because we don't need encapsulation as no other
    // actions are going to change the value of isChatting for now.
    isChatting: true,
    author: null,
    participants: []
  }
})
