const participantsTemplate =
`<div class="row">
    <ul class="list-group list-group-flush">
      <li v-for="participant in participants" class="list-group-item">
          {{ participant }}
      </li>
    </ul>
</div>`

Vue.component('participants', {
  props: ['participants'],
  created() {
  },
  mounted() {
  },
  methods: {
  },
  data() {
    return {
    }
  },
  template: participantsTemplate
})
