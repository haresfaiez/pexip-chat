const express = require('express')
const app = express()
const port = 3000

const sessionTimeOut = 3000

app.use(express.json())
app.use("/src", express.static(__dirname + '/src'));
app.use("/res", express.static(__dirname + '/res'));

const history = []

let participants = []

app.get('/participants', (req, res) => {
  const now = new Date()

  const leavings = participants
        .filter(each => !each.lastCheck || ((now - each.lastCheck) >= sessionTimeOut))
        .map(each => ({ author: 'Meetingbot', moment: new Date(), content: `${each.name} left` }))

  history.push(...leavings)

  participants = participants.filter(each => each.lastCheck && (now - each.lastCheck) < sessionTimeOut)

  console.log('Participants', participants)
  res.send(participants.map(each => each.name))
})

app.post('/participants/join', (req, res) => {
  const name = req.body.author
  const participant = participants.find(each => each.name == name)
  if (participant) {
    participant.lastCheck = new Date()
  } else {
    participants.push({ name, lastCheck: new Date() })
    history.push({ author: 'Meetingbot', moment: new Date(), content: `${name} joined` })
    console.log(req.body.author, 'joined')
  }

  res.send(participants)
})

app.get('/messages', (req, res) => {
  res.send(history)
})

app.post('/message', (req, res) => {
  if (req.body.key != undefined) {
    console.log('Got edited message', req.body)
    history[req.body.key] = Object.assign(history[req.body.key], { edit: req.body })
  } else {
    console.log('Got new message', req.body)
    history.push(req.body)
  }

  res.send(history)
})

app.delete('/message', (req, res) => {
  console.log('Deleting message', req.body)
  history[req.body.key].deleted = true
  res.send(history)
})

app.get('/', (req, res) => {
  res.sendFile('res/index.html', { root: __dirname });
})

app.listen(port, () => console.log(`Chat app listening on port ${port}!`))
